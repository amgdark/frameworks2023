
from django.contrib import admin
from django.urls import path, include
from materias.views import Bienvenida
from rest_framework import routers
from django.conf import settings
from django.conf.urls.static import static

from api import views

# METHODS
# GET POST PUT PATCH DELETE

router = routers.DefaultRouter()
router.register(r'', views.MateriaViewSet)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('programas/', include('unidades_academicas.urls_programas')),
    path('materias/', include('materias.urls')),
    path('', Bienvenida.as_view(), name='bienvenida'),
    path('usuarios/', include('usuarios.urls')),
    path('materias-api/', include(router.urls)),
    # path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
