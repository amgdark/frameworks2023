# Generated by Django 4.1.6 on 2023-02-16 17:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('unidades_academicas', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='unidadacademica',
            name='direccion',
            field=models.CharField(default='sin dirección', max_length=350, verbose_name='Dirección'),
        ),
    ]
