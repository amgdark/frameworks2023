from django import forms
from unidades_academicas.models import ProgramaAcademico

class FormProgramaAcademico(forms.ModelForm):
    
    class Meta:
        model = ProgramaAcademico
        fields = '__all__'
        # fields = ['clave, nombre']