from rest_framework import serializers
from materias.models import Materia


class SerializerMateria(serializers.ModelSerializer):
    
    programa = serializers.ReadOnlyField(source='programa.nombre')
    
    class Meta:
        model = Materia
        fields = '__all__'
        # fields = ['clave','nombre', 'creditos', 'semestre']

