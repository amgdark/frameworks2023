import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:inscripciones/main.dart';
import 'package:integration_test/integration_test.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();
  testWidgets('Correo electrónico incorrecto', (WidgetTester tester) async {
    await tester.pumpWidget(const MainApp());

    await tester.enterText(find.byKey(const Key('email')), "holamundo");
    await tester.enterText(find.byKey(const Key('password')), "admin1232");

    await tester.pumpAndSettle();

    expect(find.text('El correo es incorrecto'), findsOneWidget);
    await Future.delayed(const Duration(seconds: 2));
  });

  testWidgets('Contraseña menor a 6 caracteres', (WidgetTester tester) async {
    await tester.pumpWidget(const MainApp());

    await tester.enterText(
        find.byKey(const Key('email')), "holamundo@asdas.es");
    await tester.enterText(find.byKey(const Key('password')), "admin");

    await tester.pumpAndSettle();

    expect(
        find.text('La contraseña es de al menos 6 caracteres'), findsOneWidget);
    await Future.delayed(const Duration(seconds: 3));
  });
  testWidgets('Credenciales correctas en login', (WidgetTester tester) async {
    await tester.pumpWidget(const MainApp());

    await tester.enterText(find.byKey(const Key('email')), "alex@gmail.com");
    await tester.enterText(find.byKey(const Key('password')), "admin123");

    await tester.tap(find.byType(MaterialButton));
    await Future.delayed(const Duration(seconds: 3));

    await tester.pumpAndSettle();

    expect(find.text('HomeScreen'), findsOneWidget);
    await Future.delayed(const Duration(seconds: 2));
  });
}
