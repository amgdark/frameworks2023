Característica: Agregar materia
    Como administrador del sistema
    necesito registrar una materia
    para mantener mi plan de estudios actualizado

        Escenario: Alta exitosa
            Dado que ingreso al sistema como usario administrador
              Y selecciono el botón flotante +
              Y en el formulario agrego los siguientes datos
                  | clave | materia | creditos | semestre |
                  | 123   | Linux   | 8        | 3        |
             Cuando presiono el botón guardar
             Entonces puedo ver el mensaje: "Se guardó con éxito la materia"

        Escenario: Error al guardar la materia
            Dado que ingreso al sistema como usario administrador
              Y selecciono el botón flotante +
              Y en el formulario agrego los siguientes datos
                  | clave | materia | creditos | semestre |
                  | 123   | Linux   | 15       | 3        |
             Cuando presiono el botón guardar
             Entonces puedo ver el mensaje: "Ocurrió un error a guardar la materia"