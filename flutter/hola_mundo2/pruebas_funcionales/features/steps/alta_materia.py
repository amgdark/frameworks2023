from behave import given, when, then
from appium.webdriver.common.appiumby import AppiumBy
import time

@given(u'que ingreso al sistema como usario administrador')
def step_impl(context):
    login(context)

@given(u'selecciono el botón flotante +')
def step_impl(context):
    context.driver.find_element(by=AppiumBy.CLASS_NAME, value='android.widget.Button').click()



@given(u'en el formulario agrego los siguientes datos')
def step_impl(context):
    for row in context.table:
        cve_materia = context.driver.find_element(by=AppiumBy.XPATH, value='//android.widget.EditText[1]')
        cve_materia.clear()
        cve_materia.click()
        cve_materia.send_keys(row['clave'])
        
        materia = context.driver.find_element(by=AppiumBy.XPATH, value='//android.widget.EditText[2]')
        materia.clear()
        materia.click()
        materia.send_keys(row['materia'])
        
        creaditos = context.driver.find_element(by=AppiumBy.XPATH, value='//android.widget.EditText[3]')
        creaditos.clear()
        creaditos.click()
        creaditos.send_keys(row['creditos'])
        
        context.driver.find_element(by=AppiumBy.ACCESSIBILITY_ID, value='Semestre').click()
        time.sleep(0.5)
        context.driver.find_element(by=AppiumBy.ACCESSIBILITY_ID, value=row['semestre']).click()
        time.sleep(0.5)
        
        

@when(u'presiono el botón guardar')
def step_impl(context):
    context.driver.find_element(by=AppiumBy.ACCESSIBILITY_ID, value='Guardar').click()
    time.sleep(1)


@then(u'puedo ver el mensaje: "{mensaje}"')
def step_impl(context, mensaje):
    msn = context.driver.find_element(by=AppiumBy.ACCESSIBILITY_ID, value=mensaje)
    assert msn
    time.sleep(5)

def login(context):
    username = context.driver.find_element(by=AppiumBy.XPATH, value='//android.widget.EditText[1]')
    username.clear()
    username.click()
    username.send_keys('holamundo')
    
    contra = context.driver.find_element(by=AppiumBy.XPATH , value='//android.widget.EditText[2]')
    contra.clear()
    contra.click() ### importante dar click en la caja de texto para que tenga el foco
    time.sleep(1) ### después del click requiere algo te tiempo, para empezar a escribir
    contra.send_keys('admin123')

    context.driver.find_element(by=AppiumBy.ACCESSIBILITY_ID, value='Ingresar').click()
    time.sleep(1)
    
def inserta(componente, valor):
    componente.clear()
    componente.click()
    componente.send_keys(valor)