import 'dart:convert';
import 'package:hola_mundo2/models/models.dart';

class MateriasResponse {
  static List<Materia> obtieneMaterias(String jsonStr) {
    List<Materia> materias = [];

    for (var item in json.decode(jsonStr)) {
      materias.add(Materia(
        clave: item["clave"],
        nombre: item["nombre"],
        creditos: item["creditos"],
        semestre: item["semestre"],
        optativa: item["optativa"],
        programa: item["programa"],
      ));
    }
    return materias;
  }

  // Map<String, dynamic> toJson() => {
  //       "clave": clave,
  //       "nombre": nombre,
  //       "creditos": creditos,
  //       "semestre": semestre,
  //       "optativa": optativa,
  //       "programa": programa,
  //     };
}
