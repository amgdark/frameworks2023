import 'dart:convert';

class Materia {
  String clave;
  String nombre;
  int creditos;
  String semestre;
  bool optativa;
  String programa;

  Materia({
    required this.clave,
    required this.nombre,
    required this.creditos,
    required this.semestre,
    required this.optativa,
    required this.programa,
  });

  factory Materia.fromRawJson(String str) => Materia.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Materia.fromJson(Map<String, dynamic> json) => Materia(
        clave: json["clave"],
        programa: json["programa"],
        nombre: json["nombre"],
        creditos: json["creditos"],
        semestre: json["semestre"],
        optativa: json["optativa"],
      );

  Map<String, dynamic> toJson() => {
        "clave": clave,
        "programa": programa,
        "nombre": nombre,
        "creditos": creditos,
        "semestre": semestre,
        "optativa": optativa,
      };
}
