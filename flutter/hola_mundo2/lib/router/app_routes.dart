import 'package:flutter/material.dart';
import 'package:hola_mundo2/screens/materia_detalle_screen.dart';
import 'package:hola_mundo2/screens/materia_form2_screen.dart';
import 'package:hola_mundo2/screens/materia_form_screen.dart';
import 'package:hola_mundo2/screens/materias_screen.dart';

import '../screens/screens.dart';

class AppRoute {
  static const initialRoute = 'loginSf';

  static Map<String, Widget Function(BuildContext context)> routes = {
    'login': (BuildContext context) => LoginScreen(),
    'loginSf': (BuildContext context) => LoginViewSF(),
    'home': (BuildContext context) => const HomeScreen(),
    'listview1': (BuildContext context) => const ListView1Screen(),
    'listview2': (BuildContext context) => const ListView2Screen(),
    'alert': (BuildContext context) => const AlertScreen(),
    'card': (BuildContext context) => const CardScreen(),
    'materias': (BuildContext context) => const MateriasScreen(),
    // 'materia': (BuildContext context) => MateriaDetalle(materia: null),
    // 'materiaNueva': (BuildContext context) => MateriaFomr(),
    'materiaNueva': (BuildContext context) => const MateriaFormScreen(),
  };

  // Ruta utilizada si no se encuentra alguna dentro de routes
  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    return MaterialPageRoute(builder: (context) => const AlertScreen());
  }
}
