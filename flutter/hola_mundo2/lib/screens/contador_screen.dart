import 'package:flutter/material.dart';

class ContadorScreen extends StatelessWidget {
  const ContadorScreen({super.key});

  @override
  Widget build(BuildContext context) {
    const TextStyle estilo = TextStyle(fontSize: 30);

    return MaterialApp(
      title: 'Contador',
      home: Scaffold(
        // bottomNavigationBar: BottomNavigationBar(items: []),
        appBar: AppBar(
          title: const Text('Contador'),
          backgroundColor: const Color.fromARGB(255, 4, 186, 83),
        ),
        body: const Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Número de Clicks:', style: estilo),
              Text(
                '0',
                style: estilo,
              ),
            ],
          ),
        ),
        floatingActionButton: const Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            FloatingActionButton(
              onPressed: null,
              tooltip: 'Increment',
              child: Icon(Icons.add),
            ),
            FloatingActionButton(
              onPressed: null,
              tooltip: 'Reset',
              child: Icon(Icons.reset_tv),
            ),
            FloatingActionButton(
              onPressed: null,
              tooltip: 'Decrement',
              child: Icon(Icons.minimize),
            ),
          ],
        ),
      ),
    );
  }
}
