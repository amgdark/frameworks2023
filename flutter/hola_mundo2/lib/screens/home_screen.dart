import 'package:flutter/material.dart';
import 'package:hola_mundo2/screens/screens.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home screen'),
      ),
      body: Center(
          child: ListView.separated(
              itemBuilder: (context, index) => ListTile(
                  title: const Text('data'),
                  trailing: const Icon(Icons.arrow_forward_ios_outlined),
                  onTap: () {
                    // final route = MaterialPageRoute(
                    //     builder: (context) => const ListView1Screen());
                    // Navigator.push(context, route);
                    Navigator.pushNamed(context, 'card');
                  }),
              separatorBuilder: ((context, index) => const Divider()),
              itemCount: 10)),
    );
  }
}
