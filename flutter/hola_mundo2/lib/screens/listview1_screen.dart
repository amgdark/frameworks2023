import 'package:flutter/material.dart';

class ListView1Screen extends StatelessWidget {
  const ListView1Screen({super.key});

  final List<String> materias = const [
    'Linux',
    'Administración',
    'Testing',
    'Deployment',
    'Frameworks'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Listview 1'),
        ),
        body: ListView(
            children: materias
                .map((item) => ListTile(
                      title: Text(item),
                      trailing: const Icon(Icons.arrow_forward_ios_outlined),
                    ))
                .toList()));
  }
}
