import 'package:flutter/material.dart';

class ListView2Screen extends StatelessWidget {
  const ListView2Screen({super.key});

  final List<String> materias = const [
    'Linux',
    'Administración',
    'Testing',
    'Deployment',
    'Frameworks'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Listview 2'),
        elevation: 2,
        backgroundColor: Colors.indigo,
      ),
      body: ListView.separated(
          itemBuilder: (context, index) => ListTile(
                title: Text(materias[index]),
                trailing: const Icon(
                  Icons.arrow_forward_ios_outlined,
                  color: Colors.indigo,
                ),
                onTap: () => print(materias[index]),
              ),
          separatorBuilder: (_, __) => const Divider(),
          itemCount: materias.length),
    );
  }
}
