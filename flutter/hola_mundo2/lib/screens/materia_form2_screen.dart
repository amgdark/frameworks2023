import 'dart:ffi';

import 'package:flutter/material.dart';

import '../widgets/input_decorations.dart';

class MateriaFormScreen extends StatelessWidget {
  const MateriaFormScreen({super.key});

  @override
  Widget build(BuildContext context) {
    List<String> valores = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];

    return Scaffold(
      appBar: AppBar(
        title: const Text('Materia'),
      ),
      body: Container(
        child: Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  TextFormField(
                      key: const Key('claveMateria'),
                      autofocus: true,
                      autocorrect: false,
                      keyboardType: TextInputType.number,
                      decoration: InputDecorations.fieldInputDecoration(
                          hintText: '123',
                          labelText: 'Clave de la materia',
                          prefixIcon: Icons.key_off_outlined),
                      validator: (value) {
                        String pattern = r'^(\d+)$';
                        RegExp regExp = RegExp(pattern);

                        return regExp.hasMatch(value ?? '')
                            ? null
                            : 'La clave sólo debete de contener números';
                      }),
                  const SizedBox(
                    height: 30,
                  ),
                  TextFormField(
                      key: const Key('nombreMateria'),
                      autofocus: true,
                      autocorrect: false,
                      keyboardType: TextInputType.text,
                      decoration: InputDecorations.fieldInputDecoration(
                          hintText: 'Sistema Operativo Linux',
                          labelText: 'Nombre de la materia',
                          prefixIcon: Icons.subject_outlined),
                      validator: (value) {
                        if (value == null) return 'Este campo es requerido';
                        return value.length < 3 ? 'Mínimo 3 letras' : null;
                      }),
                  const SizedBox(
                    height: 30,
                  ),
                  TextFormField(
                      key: const Key('creditosMateria'),
                      autofocus: true,
                      autocorrect: false,
                      keyboardType: TextInputType.number,
                      decoration: InputDecorations.fieldInputDecoration(
                          hintText: '10',
                          labelText: 'Créditos de la materia',
                          prefixIcon: Icons.credit_score),
                      validator: (value) {
                        if (value == null) return 'Este campo es requerido';
                        // return (int.parse(value) > 0 && int.parse(value) <= 12)
                        //     ? 'Los créditos deben de estar en el rango de 1 a 12'
                        //     : null;
                      }),
                  const SizedBox(
                    height: 30,
                  ),
                  DropdownButtonFormField(
                    key: const Key('semestre'),
                    decoration: InputDecorations.fieldInputDecoration(
                        hintText: 'Semestre',
                        labelText: 'Semestre',
                        prefixIcon: Icons.grade_outlined),
                    items: valores.map((String val) {
                      return DropdownMenuItem(
                        value: val,
                        child: Text(
                          val,
                        ),
                      );
                    }).toList(),
                    onChanged: (value) {
                      // semestre = value!;

                      // print(value);
                    },
                  ),
                  const SizedBox(height: 20),
                  SwitchListTile(
                    value: true,
                    title: const Text('Obligatoria'),
                    onChanged: (value) {},
                  ),
                  const SizedBox(height: 20),
                  MaterialButton(
                    onPressed: () {},
                    // onPressed:
                    //     loginForm.isLoading // si está cargando desactiva el botón
                    //         ? null
                    //         : () async {
                    //             FocusScope.of(context)
                    //                 .unfocus(); // esconde el teclado al presionar el botón

                    //             if (!loginForm.isValidForm()) return;
                    //             loginForm.isLoading = true;

                    //             await Future.delayed(const Duration(seconds: 2));
                    //             loginForm.isLoading = false;

                    //             Navigator.pushReplacementNamed(context, '/home');
                    //           },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    disabledColor: Colors.grey,
                    elevation: 0,
                    color: Colors.deepPurple,
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 80, vertical: 15),
                      child: const Text(
                        'Guardar',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
