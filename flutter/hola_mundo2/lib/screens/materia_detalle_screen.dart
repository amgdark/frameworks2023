import 'package:flutter/material.dart';
import 'package:hola_mundo2/screens/materia_form_screen.dart';

import '../models/models.dart';

class MateriaDetalle extends StatelessWidget {
  const MateriaDetalle({super.key, required this.materia});

  final Materia materia;

  @override
  Widget build(BuildContext context) {
    const TextStyle estiloEtiqueta =
        TextStyle(fontSize: 20, fontWeight: FontWeight.w100);
    const TextStyle estiloValor =
        TextStyle(fontSize: 20, fontWeight: FontWeight.bold);
    return Scaffold(
        appBar: AppBar(title: Text('Detalle ${materia.nombre}')),
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    const Text('Clave:', style: estiloEtiqueta),
                    Text(materia.clave, style: estiloValor),
                  ],
                ),
                const SizedBox(height: 40),
                Row(
                  children: [
                    const Text('Nombre:', style: estiloEtiqueta),
                    Text(materia.nombre, style: estiloValor),
                  ],
                ),
                const SizedBox(height: 40),
                Row(
                  children: [
                    const Text('Semestre:', style: estiloEtiqueta),
                    Text(materia.semestre, style: estiloValor),
                  ],
                ),
                const SizedBox(height: 40),
                Row(
                  children: [
                    const Text('Créditos:', style: estiloEtiqueta),
                    Text('${materia.creditos}', style: estiloValor),
                  ],
                ),
                const SizedBox(height: 40),
                Row(
                  children: [
                    const Text(
                      'Programa:',
                      style: estiloEtiqueta,
                    ),
                    Flexible(
                      child: Text(materia.programa, style: estiloValor),
                    ),
                  ],
                ),
                const SizedBox(height: 40),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text('Regresar')),
                    ElevatedButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text('Editar')),
                  ],
                )
              ],
            ),
          ),
        ));
  }
}
