import 'package:flutter/material.dart';
import 'package:hola_mundo2/models/models.dart';
import 'package:hola_mundo2/providers/materias_provider.dart';
import 'package:hola_mundo2/screens/materia_detalle_screen.dart';
import 'package:provider/provider.dart';

class MateriasScreen extends StatelessWidget {
  const MateriasScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final materiasProvider = Provider.of<MateriasProvider>(context);
    List<Materia> materias = materiasProvider.materias;
    print(materias);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Materias'),
      ),
      body: ListView(
          children: materias
              .map((materia) => ListTile(
                    leading: const Icon(Icons.subject_outlined),
                    title: Text(materia.nombre),
                    subtitle: Text(materia.programa),
                    trailing: const Icon(Icons.arrow_forward_ios_outlined),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MateriaDetalle(
                                    materia: materia,
                                  )));
                    },
                  ))
              .toList()),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final refrescar = await Navigator.pushNamed(context, 'materiaNueva');

          if (refrescar == true) {
            materias = materiasProvider.materias;
          }
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
