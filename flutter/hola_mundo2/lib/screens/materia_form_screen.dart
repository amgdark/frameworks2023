import 'package:flutter/material.dart';

String guardar(String clave, String creditos, String materia, String semestre) {
  // print('Clave: $clave, semestre: $semestre, materia: $materia, creditos: $creditos ');
  if (int.parse(creditos) > 12) {
    return 'error';
  }
  return 'ok';
}

class MateriaFomr extends StatelessWidget {
  MateriaFomr({super.key});

  final TextEditingController claveController = TextEditingController();
  final TextEditingController materiaController = TextEditingController();
  final TextEditingController creditosController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    List<String> valores = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
    String semestre = '';
    return Scaffold(
      appBar: AppBar(
        title: const Text('Guardar materia'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
            children: [
              TextFormField(
                keyboardType: TextInputType.text,
                key: const Key('clave'),
                autofocus: true,
                controller: claveController,
                // initialValue: "Materia",
                textCapitalization: TextCapitalization.sentences,
                onChanged: (value) {
                  print('Valor: $value');
                },
                validator: (value) {
                  if (value == null) return 'Este campo es requerido';
                  return value.length < 3 ? 'Mínimo 3 letras' : null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: const InputDecoration(
                    hintText: 'Clave de la materia',
                    labelText: 'Clave',
                    helperText: 'Ingresa la clave de la materia',
                    prefixIcon: Icon(Icons.key_rounded),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(10),
                            topRight: Radius.circular(10)))),
              ),
              const SizedBox(
                height: 30,
              ),
              TextFormField(
                // autofocus: true,
                // initialValue: "Materia",
                key: const Key('materia'),

                controller: materiaController,
                keyboardType: TextInputType.text,
                textCapitalization: TextCapitalization.sentences,
                onChanged: (value) {
                  print('Valor: $value');
                },
                validator: (value) {
                  if (value == null) return 'Este campor es requerido';
                  return value.length < 3 ? 'Mínimo 3 letras' : null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: const InputDecoration(
                    hintText: 'Nombre de la materia',
                    labelText: 'Nombre',
                    helperText: 'Ingresa el nombre de la materia',
                    prefixIcon: Icon(Icons.abc),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(10),
                            topRight: Radius.circular(10)))),
              ),
              const SizedBox(
                height: 30,
              ),
              TextFormField(
                // autofocus: true,
                // initialValue: "Materia",
                key: const Key('creditos'),

                controller: creditosController,
                keyboardType: TextInputType.number,
                textCapitalization: TextCapitalization.sentences,
                onChanged: (value) {
                  print('Valor: $value');
                },
                validator: (value) {
                  if (value == null) return 'Este campo es requerido';
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: const InputDecoration(
                    hintText: 'Créditos de la materia',
                    labelText: 'Créditos',
                    helperText: 'Ingresa los créditos de la materia',
                    prefixIcon: Icon(Icons.credit_score),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(10),
                            topRight: Radius.circular(10)))),
              ),
              const SizedBox(
                height: 30,
              ),
              DropdownButtonFormField(
                key: const Key('semestre'),
                decoration: const InputDecoration(
                    hintText: 'Semestre de la materia',
                    labelText: 'Semestre',
                    helperText: 'Selecciona el semestre de la materia',
                    prefixIcon: Icon(Icons.credit_score),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(10),
                            topRight: Radius.circular(10)))),
                items: valores.map((String val) {
                  return DropdownMenuItem(
                    value: val,
                    child: Text(
                      val,
                    ),
                  );
                }).toList(),
                onChanged: (value) {
                  semestre = value!;

                  print(value);
                },
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.all(25),
                child: ElevatedButton(
                    key: const Key('botonAgregar'),
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.purple,
                        foregroundColor: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(
                          10,
                        ))),
                    onPressed: () {
                      String res = guardar(
                          claveController.text.toString(),
                          creditosController.text.toString(),
                          materiaController.text,
                          semestre);
                      String contenido = "Se guardó con éxito la materia";
                      String titulo = "Éxito";
                      Color colorBg = const Color.fromARGB(255, 122, 177, 151);
                      ElevatedButton boton = ElevatedButton(
                          onPressed: () {
                            print('todo ok');

                            Navigator.of(context).pop(true);
                            Navigator.of(context).pop(true);
                          },
                          child: const Text("Ok"));
                      if (res != 'ok') {
                        contenido = "Ocurrió un error a guardar la materia";
                        titulo = "Error";
                        colorBg = const Color.fromARGB(255, 233, 138, 112);
                        boton = ElevatedButton(
                            onPressed: () {
                              print('error');
                              Navigator.of(context).pop(false);
                            },
                            child: const Text("Ok"));
                      }
                      showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text(titulo),
                              content: Text(contenido),
                              titleTextStyle: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                  fontSize: 20),
                              backgroundColor: colorBg,
                              actions: [boton],
                              shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            );
                          });
                      //
                    },
                    child: const Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Text(
                        'Guardar',
                        style: TextStyle(fontSize: 23),
                      ),
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
