import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginViewSF extends StatefulWidget {
  const LoginViewSF({super.key});

  @override
  State<LoginViewSF> createState() => _LoginViewSFState();
}

class _LoginViewSFState extends State<LoginViewSF> {
  final TextEditingController controllerUser = TextEditingController();
  final TextEditingController controllerPassword = TextEditingController();
  String mensaje = '';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: Scaffold(
        backgroundColor: Colors.blue[100],
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Icon(
                  Icons.school,
                  size: 100,
                ),
                const SizedBox(height: 20),
                Text(
                  'Ingeniería de Sofware',
                  style: GoogleFonts.acme(fontSize: 35),
                ),
                const SizedBox(height: 12),
                const Text(
                  'Bienvenido!!',
                  style: TextStyle(
                    fontSize: 24,
                  ),
                ),
                const SizedBox(height: 40),
                Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 25.0, vertical: 5),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Colors.white12),
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: TextFormField(
                      key: const Key('username'),
                      controller: controllerUser,
                      autofocus: true,
                      decoration: const InputDecoration(
                          border: InputBorder.none, hintText: 'Email'),
                    ),
                  ),
                ),
                Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 25.0, vertical: 5),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Colors.white12),
                      borderRadius: BorderRadius.circular(12)),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: TextFormField(
                      key: const Key('password'),
                      controller: controllerPassword,
                      obscureText: true,
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Password',
                      ),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: const EdgeInsets.all(25),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.purple,
                          foregroundColor: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(
                            10,
                          ))),
                      onPressed: () {
                        print('hola');
                        // Navigator.pushReplacementNamed(context, 'materias');
                        // Navigator.push(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (context) => const MateriasScreen()));
                        login(context, controllerUser.text.toString(),
                            controllerPassword.text.toString());
                      },
                      child: const Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text(
                          'Ingresar',
                          style: TextStyle(fontSize: 23),
                        ),
                      )),
                ),
                Text(
                  mensaje,
                  style: const TextStyle(fontSize: 23),
                  key: const Key('mensaje'),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      '¿No tienes cuenta? ',
                      style: TextStyle(fontSize: 15),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: const Text(
                        ' Crea una',
                        style: TextStyle(color: Colors.blue, fontSize: 16),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void login(contexto, String username, String password) {
    if (username == 'holamundo' && password == 'admin123') {
      print("Login success");
      mensaje = 'Login success';
      Navigator.pushReplacementNamed(contexto, 'materias');
    } else {
      print('login fail');
      mensaje = 'Login fail';
    }
    setState(() {});
  }
}
