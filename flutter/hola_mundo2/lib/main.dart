import 'package:flutter/material.dart';
import 'package:hola_mundo2/providers/materias_provider.dart';
import 'package:hola_mundo2/router/app_routes.dart';
import 'package:hola_mundo2/screens/login_screen_sf.dart';
import 'package:provider/provider.dart';

// Para  convertir un json a una clase/modelo
// https://app.quicktype.io/

void main() {
  // runApp(const LoginScreen());
  // runApp(const MyApp());
  runApp(const AppState());
  // runApp(const MaterialApp(
  //   home: AppState(),
  //   debugShowCheckedModeBanner: false,
  // ));
}

class AppState extends StatelessWidget {
  const AppState({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: [
      ChangeNotifierProvider(
        create: (_) => MateriasProvider(),
        lazy: false,
      )
    ], child: const MyApp());
  }
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Mi Aplicación',
        // home: const ListView2Screen(),
        debugShowCheckedModeBanner: false,
        initialRoute: AppRoute.initialRoute,
        routes: AppRoute.routes,
        // routes: {
        //    'login': (BuildContext context) => const LoginScreen(),
        //   'home': (BuildContext context) => const HomeScreen(),
        //   'listview1': (BuildContext context) => const ListView1Screen(),
        //   'listview2': (BuildContext context) => const ListView2Screen(),
        //   'alert': (BuildContext context) => const AlertScreen(),
        //   'card': (BuildContext context) => const CardScreen(),
        // },
        // onGenerateRoute: (settings) => AppRoute.onGenerateRoute(settings)));
        // La línea de arriba tiene el mismo efecto, sólo que aquí se envía por referencia
        // al recibir y enviar el mismo parámetro (settings) se puede obviar por referencia
        onGenerateRoute: AppRoute.onGenerateRoute);
  }
}
