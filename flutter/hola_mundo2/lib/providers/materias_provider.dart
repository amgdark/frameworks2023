import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../models/models.dart';

class MateriasProvider extends ChangeNotifier {
  final String _baseurl = '10.2.224.33:8000';
  final String _user = 'alex';
  final String _pass = 'alexmau1';

  List<Materia> materias = [];

  MateriasProvider() {
    obtieneMaterias();
  }

  obtieneMaterias() async {
    final url = Uri.http('$_user:$_pass@$_baseurl', '/materias-api/', {});
    try {
      final response = await http.get(url);
      materias = MateriasResponse.obtieneMaterias(response.body);
    } catch (e) {
      print('error en la conexión' + e.toString());
    }

    // print(response.statusCode);
    // print(materias[0].programa);

    notifyListeners();
  }
}
