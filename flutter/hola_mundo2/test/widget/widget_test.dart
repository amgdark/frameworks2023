import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hola_mundo2/screens/login_screen_sf.dart';

void main() {
  testWidgets('Credenciales incorrectas en login', (WidgetTester tester) async {
    await tester.pumpWidget(const LoginViewSF());

    await tester.enterText(find.byKey(const Key('username')), "holamundo");
    await tester.enterText(find.byKey(const Key('password')), "admin1232");

    await tester.tap(find.byType(ElevatedButton));

    await tester.pump();

    expect(find.text('Login fail'), findsOneWidget);

    // await tester.tap(find.byIcon(Icons.add));
    // await tester.pump();

    // expect(find.text('0'), findsNothing);
    // expect(find.text('1'), findsOneWidget);
  });
}
