import 'package:test/test.dart';
import 'package:hola_mundo2/screens/materia_form_screen.dart' as materia;

void main() {
  test('smoke test', () {
    expect(2, 2);
  });

  group('grupo de pruebas para el método guardar materia', () {
    test('guarda materia', () {
      final res = materia.guardar('123', '8', 'Linux', '8');
      expect(res, 'ok');
    });

    test('error al guardar materia, créditos mayor a 12', () {
      final res = materia.guardar('123', '15', 'Linux', '8');
      expect(res, 'error');
    });
  });
}
